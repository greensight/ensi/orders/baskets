<?php

namespace App\Domain\Baskets\Tests;

use App\Domain\Common\Tests\Factories\Catalog\OffersOfferFactory;
use App\Domain\Common\Tests\Factories\Catalog\ProductFactory;
use App\Domain\Common\Tests\Factories\Catalog\StockFactory;
use Mockery\MockInterface;
use Tests\ComponentTestCase;

/**
 * @mixin ComponentTestCase
 */
trait BasketSetItemTestCase
{
    public function mockSetItem(
        array $products,
        array $offers,
        MockInterface $mockOffersOffersApi = null,
    ): void {
        $this->mockPimProductsApi()->allows([
            'searchProducts' => ProductFactory::new()->makeResponseSearch($products, count: count($products)),
        ]);

        $offersResponse = [];
        foreach ($offers as $offer) {
            if (is_array($offer)) {
                $offerFactory = OffersOfferFactory::new();
                if (array_key_exists('qty', $offer)) {
                    $offerFactory->withStock(StockFactory::new()->make(['qty' => $offer['qty']]));
                    unset($offer['qty']);
                }
                $offersResponse[] = $offerFactory->make($offer);
            } else {
                $offersResponse[] = $offer;
            }
        }

        ($mockOffersOffersApi ?: $this->mockOffersOffersApi())
            ->shouldReceive('searchOffers')
            ->andReturn(OffersOfferFactory::new()->makeResponseSearch($offersResponse));
    }
}
