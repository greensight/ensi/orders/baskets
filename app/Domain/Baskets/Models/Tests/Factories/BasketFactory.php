<?php

namespace App\Domain\Baskets\Models\Tests\Factories;

use App\Domain\Baskets\Models\Basket;
use Ensi\LaravelTestFactories\BaseModelFactory;

class BasketFactory extends BaseModelFactory
{
    protected $model = Basket::class;

    public function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'promo_code' => $this->faker->optional()->text(10),
        ];
    }
}
