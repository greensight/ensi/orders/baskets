<?php

namespace App\Domain\Baskets\Models;

use App\Domain\Baskets\Models\Tests\Factories\BasketFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property int $customer_id Customer ID
 * @property string|null $promo_code Add the promo code to the basket
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read Collection|BasketItem[] $items Basket items
 */
class Basket extends Model
{
    protected $table = 'baskets';

    public static function factory(): BasketFactory
    {
        return BasketFactory::new();
    }

    public function items(): HasMany
    {
        return $this->hasMany(BasketItem::class);
    }

    public static function makeForCustomer(int $customerId): self
    {
        $basket = new Basket();
        $basket->customer_id = $customerId;
        $basket->setRelation('items', new Collection());

        return $basket;
    }
}
