<?php

namespace App\Domain\Baskets\Actions\Basket;

use App\Domain\Baskets\Actions\Basket\Data\BasketPromoCodeData;
use App\Domain\Baskets\Models\Basket;
use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\MarketingClient\Dto\CheckPromoCodeRequest;
use Ensi\MarketingClient\Dto\CheckPromoCodeResponseData;
use Ensi\MarketingClient\Dto\PromoCodeApplyStatusEnum;

class SetBasketPromoCodeAction
{
    public function __construct(protected PromoCodesApi $promoCodesApi)
    {
    }

    public function execute(int $customerId, ?string $promoCode): BasketPromoCodeData
    {
        $promoCode = $promoCode ? mb_strtolower($promoCode) : null;
        $applyStatus = null;

        /** @var Basket|null $basket */
        $basket = Basket::query()->where('customer_id', $customerId)->first();

        if (!$basket) {
            return new BasketPromoCodeData(applyStatus: PromoCodeApplyStatusEnum::NOT_APPLIED);
        }

        if ($basket->promo_code == $promoCode) {
            return new BasketPromoCodeData(applyStatus: PromoCodeApplyStatusEnum::SUCCESS);
        }

        if ($promoCode) {
            $promoCodeData = $this->getPromoCodeData($promoCode);
            if (!$promoCodeData->getIsAvailable()) {
                $promoCode = null;
                $applyStatus = $promoCodeData->getPromoCodeApplyStatus();
            }
        }

        $basket->promo_code = $promoCode;
        if ($basket->isDirty()) {
            $basket->save();
        }

        return new BasketPromoCodeData(applyStatus: $applyStatus ?? PromoCodeApplyStatusEnum::SUCCESS, basket: $basket);
    }

    protected function getPromoCodeData(string $promoCode): CheckPromoCodeResponseData
    {
        $request = new CheckPromoCodeRequest();
        $request->setPromoCode($promoCode);

        return $this->promoCodesApi->checkPromoCode($request)->getData();
    }
}
