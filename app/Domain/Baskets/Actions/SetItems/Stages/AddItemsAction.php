<?php

namespace App\Domain\Baskets\Actions\SetItems\Stages;

use App\Domain\Baskets\Actions\SetItems\Data\SetItemsContext;
use App\Domain\Baskets\Models\BasketItem;
use App\Exceptions\ValidateException;

class AddItemsAction
{
    public function __construct(
        protected LoadProductInfoAction $loadProductInfoAction,
    ) {
    }

    public function execute(SetItemsContext $context): void
    {
        if (!$context->data->addItems()) {
            return;
        }
        $this->loadProductInfoAction->execute($context);

        $basket = $context->getBasketOrCreate();
        $basketItems = $basket->items->keyBy('offer_id');
        foreach ($context->data->addItems() as $itemData) {
            $productData = $context->getProductInfo($itemData->offerId);

            if ($productData->getMinQty() > $itemData->qty) {
                throw new ValidateException(
                    "Для товара {$productData->offer->getId()} нельзя установить количество меньше {$productData->getMinQty()}"
                );
            }

            /** @var BasketItem|null $item */
            $item = $basketItems->get($itemData->offerId);
            if (!$item) {
                $item = new BasketItem();
                $item->offer_id = $itemData->offerId;
                $item->basket_id = $basket->id;
                $item->product_id = $productData->offer->getProductId();

                $item->setRelation('basket', $basket);
                $item->makeHidden('basket');
            }

            $item->qty = $itemData->qty;

            $item->save();
        }
    }
}
