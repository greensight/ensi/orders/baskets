<?php

namespace App\Domain\Baskets\Actions\Calculators\Stages;

use App\Domain\Baskets\Actions\Calculators\Data\CalculatorBasketContext;

class PromoCodeUpdateAction
{
    public function execute(CalculatorBasketContext $context): void
    {
        if ($context->basket->promo_code != $context->usedPromoCode) {
            $context->basket->promo_code = $context->usedPromoCode;
            $context->basket->save();
        }
    }
}
