<?php

namespace App\Domain\Kafka\Messages\Listen\Order;

use Illuminate\Support\Fluent;

/**
 * Class OrderPayload
 * @package App\Domain\Kafka\Payloads\Listen
 *
 * @property int $id
 * @property int $customer_id
 */

class OrderPayload extends Fluent
{
}
