<?php

namespace App\Domain\Common\Tests\Factories\Marketing;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\CalculatedDiscount;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;

class CalculatedDiscountFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'value_type' => $this->faker->randomElement(DiscountValueTypeEnum::getAllowableEnumValues()),
            'value' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): CalculatedDiscount
    {
        return new CalculatedDiscount($this->makeArray($extra));
    }
}
