<?php

namespace App\Domain\Common\Tests\Factories\Marketing;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\CalculateCheckoutResponse;
use Ensi\MarketingClient\Dto\CalculateCheckoutResponseData;
use Ensi\MarketingClient\Dto\PromoCodeApplyStatusEnum;

class CalculateCheckoutFactory extends BaseApiFactory
{
    protected array $offers = [];

    protected function definition(): array
    {
        return [
            'offers' => $this->makeOffers(),
            'promo_code' => $this->faker->nullable()->word(),
            'promo_code_apply_status' => $this->faker->nullable()->randomElement(PromoCodeApplyStatusEnum::getAllowableEnumValues()),
        ];
    }

    protected function makeOffers(): array
    {
        $count = $this->offers ? count($this->offers) : $this->faker->numberBetween(1, 10);
        $offers = [];
        for ($i = 0; $i < $count; $i++) {
            $offers[] = CalculatedOfferFactory::new()->make($this->offers[$i] ?? []);
        }

        return $offers;
    }

    public function withOffer(array $offer): self
    {
        $this->offers[] = $offer;

        return $this;
    }

    public function withOffers(array $offers): self
    {
        $this->offers = array_merge($this->offers, $offers);

        return $this;
    }

    public function make(array $extra = []): CalculateCheckoutResponseData
    {
        return new CalculateCheckoutResponseData($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CalculateCheckoutResponse
    {
        return new CalculateCheckoutResponse(['data' => $this->make($extra)]);
    }
}
