<?php

namespace App\Domain\Common\Tests\Factories\Marketing;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\CheckPromoCodeResponse;
use Ensi\MarketingClient\Dto\CheckPromoCodeResponseData;
use Ensi\MarketingClient\Dto\PromoCodeApplyStatusEnum;

class CheckPromoCodeFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'is_available' => $this->faker->boolean(),
            'promo_code_apply_status' => $this->faker->randomElement(PromoCodeApplyStatusEnum::getAllowableEnumValues()),
        ];
    }

    public function make(array $extra = []): CheckPromoCodeResponseData
    {
        return new CheckPromoCodeResponseData($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CheckPromoCodeResponse
    {
        return new CheckPromoCodeResponse(['data' => $this->make($extra)]);
    }
}
