<?php

namespace App\Http\ApiV1\Modules\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatchSettingsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'settings' => ['required', 'min:1'],
            'settings.*.id' => ['required', 'integer'],
            'settings.*.name' => ['string'],
            'settings.*.value' => ['string'],
        ];
    }
}
