<?php

namespace App\Http\ApiV1\Modules\Baskets\Resources\Baskets;

use App\Domain\Baskets\Models\Basket;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Basket */
class BasketsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'promo_code' => $this->promo_code,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'items' => BasketItemsResource::collection($this->whenLoaded('items')),
        ];
    }
}
