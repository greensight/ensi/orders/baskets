<?php

namespace App\Http\ApiV1\Modules\Baskets\Resources\Calculators;

use App\Domain\Baskets\Actions\Calculators\Data\CalculatorBasketContext;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin CalculatorBasketContext */
class CalculateBasketsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'items' => CalculateBasketItemsResource::collection($this->items()),
            'promo_code' => $this->basket?->promo_code,
            'promo_code_apply_status' => $this->promoCodeApplyStatus,
        ];
    }
}
