<?php

use App\Domain\Baskets\Models\Basket;
use App\Domain\Baskets\Models\BasketItem;
use App\Domain\Baskets\Tests\BasketCalculateTestCase;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Carbon\CarbonImmutable;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\MarketingClient\Dto\PromoCodeApplyStatusEnum;
use Illuminate\Support\Collection;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses(BasketCalculateTestCase::class);
uses()->group('component');

test('POST /api/v1/baskets/baskets/calculate:search-one 200 one items', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase|BasketCalculateTestCase $this */
    $offerId = 1;
    $productId = 3;
    $qty = 2;
    $stockQty = $qty + 1;
    $pricePerOne = 100;
    $costPerOne = 100;
    $discountPerOne = $pricePerOne - $costPerOne;
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    BasketItem::factory()->for($basket)->create([
        'offer_id' => $offerId,
        'product_id' => $productId,
        'qty' => $qty,
    ]);

    $this->mockCalculate(
        catalogCacheOffers: [
            ['id' => $offerId, 'product_id' => $productId],
        ],
        offers: [
            ['id' => $offerId, 'qty' => $stockQty],
        ],
        calculatedOffers: [
            ['offer_id' => $offerId, 'price' => $pricePerOne, 'cost' => $costPerOne],
        ],
        promoCode: $basket->promo_code,
    );

    postJson('/api/v1/baskets/baskets/calculate:search-one', ["customer_id" => $basket->customer_id])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data.items')
        ->assertJsonPath('data.items.0.offer_id', $offerId)
        ->assertJsonPath('data.items.0.product_id', $productId)
        ->assertJsonPath('data.items.0.price_per_one', $pricePerOne)
        ->assertJsonPath('data.items.0.price', $pricePerOne * $qty)
        ->assertJsonPath('data.items.0.cost_per_one', $costPerOne)
        ->assertJsonPath('data.items.0.cost', $costPerOne * $qty)
        ->assertJsonPath('data.items.0.discount_per_one', $discountPerOne)
        ->assertJsonPath('data.items.0.discount', $discountPerOne * $qty)
        ->assertJsonPath('data.items.0.qty', $qty)
        ->assertJsonPath('data.items.0.stock_qty', $stockQty)
        ->assertJsonPath('data.promo_code', $basket->promo_code);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/baskets/baskets/calculate:search-one 404', function () {
    postJson('/api/v1/baskets/baskets/calculate:search-one', ["customer_id" => 404])
        ->assertStatus(404);
});

test("POST /api/v1/baskets/baskets/calculate:search-one check sort items", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase|BasketCalculateTestCase $this */
    $count = 3;
    $stockQty = 5;
    $createAt = CarbonImmutable::now();

    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    /** @var BasketItem $oldBasketItem */
    $oldBasketItem = BasketItem::factory()->for($basket)->create(['created_at' => $createAt->subSecond(), 'offer_id' => $count + 1]);

    /** @var Collection<BasketItem> $basketItems */
    $basketItems = collect();
    for ($i = 0; $i < $count; $i++) {
        $basketItems->add(BasketItem::factory()->for($basket)->create(['created_at' => $createAt, 'offer_id' => $count - $i]));
    }

    $offerIds = $basketItems->pluck('offer_id');
    $offerIds->add($oldBasketItem->offer_id);

    $this->mockCalculate(
        catalogCacheOffers: $offerIds->map(fn (int $offerId) => (['id' => $offerId]))->toArray(),
        offers: $offerIds->map(fn (int $offerId) => (['id' => $offerId, 'qty' => $stockQty]))->toArray(),
        calculatedOffers: $offerIds->map(fn (int $offerId) => (['offer_id' => $offerId]))->toArray()
    );

    $response = postJson('/api/v1/baskets/baskets/calculate:search-one', ["customer_id" => $basket->customer_id])
        ->assertStatus(200);

    $response->assertJsonPath("data.items.0.offer_id", $oldBasketItem->offer_id);
    foreach ($basketItems->reverse()->values() as $key => $basketItem) {
        $keyData = $key + 1;
        $response->assertJsonPath("data.items.{$keyData}.offer_id", $basketItem->offer_id);
    }
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/baskets/baskets/calculate:search-one reset promo code', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase|BasketCalculateTestCase $this */
    $offerId = 1;
    $productId = 3;
    $qty = 2;
    $stockQty = $qty + 1;
    /** @var Basket $basket */
    $basket = Basket::factory()->create(['promo_code' => 'promo_code']);
    BasketItem::factory()->for($basket)->create([
        'offer_id' => $offerId,
        'product_id' => $productId,
        'qty' => $qty,
    ]);

    $this->mockCalculate(
        catalogCacheOffers: [
            ['id' => $offerId, 'product_id' => $productId],
        ],
        offers: [
            ['id' => $offerId, 'qty' => $stockQty],
        ],
        calculatedOffers: [
            ['offer_id' => $offerId],
        ],
        applyStatus: PromoCodeApplyStatusEnum::NOT_APPLIED,
    );

    postJson('/api/v1/baskets/baskets/calculate:search-one', ["customer_id" => $basket->customer_id])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data.items')
        ->assertJsonPath('data.items.0.offer_id', $offerId)
        ->assertJsonPath('data.items.0.product_id', $productId)
        ->assertJsonPath('data.promo_code', null);
})->with(FakerProvider::$optionalDataset);
