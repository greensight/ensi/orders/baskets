<?php

use App\Domain\Baskets\Models\Basket;
use App\Domain\Baskets\Models\BasketItem;
use App\Domain\Baskets\Tests\BasketSetItemTestCase;
use App\Http\ApiV1\Modules\Baskets\Tests\Factories\BasketCustomerSetItemRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\PimClient\Dto\ProductTypeEnum;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses(BasketSetItemTestCase::class);
uses()->group('component');

test("POST /api/v1/baskets/baskets/customer:search-one success", function () {
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    $basketItems = BasketItem::factory()->for($basket)->count(3)->create();

    postJson("/api/v1/baskets/baskets/customer:search-one", [
        "customer_id" => $basket->customer_id,
        "include" => ["items"],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $basket->id)
        ->assertJsonCount($basketItems->count(), 'data.items');
});

test("POST /api/v1/baskets/baskets/customer:search-one 404", function () {
    postJson("/api/v1/baskets/baskets/customer:search-one", ["customer_id" => 2])
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("POST /api/v1/baskets/baskets/customer:set-item success", function () {
    /** @var ApiV1ComponentTestCase|BasketSetItemTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    $offerId = 1;
    $productId = 1;
    $qty = 5.4;

    $this->mockSetItem(
        products: [[
            'id' => $productId,
            'order_minvol' => $qty - 1,
        ]],
        offers: [[
            'id' => $offerId,
            'product_id' => $productId,
            'qty' => $qty + 1,
        ]],
    );

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty, $offerId)
        ->withCustomer($basket->customer_id)
        ->make();

    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseHas(BasketItem::class, [
        "basket_id" => $basket->id,
        "offer_id" => $offerId,
        "qty" => $qty,
    ]);
});

test("POST /api/v1/baskets/baskets/customer:set-item many success", function (
    bool $isRealActive1 = true,
    bool $isRealActive2 = true,
    int $status = 200
) {
    /** @var ApiV1ComponentTestCase|BasketSetItemTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    $offer1Id = 1;
    $offer2Id = 2;
    $productId = 2;
    $qty1 = 5.4;
    $qty2 = 10;
    $minQty = min($qty1, $qty2) - 1;

    $this->mockSetItem(
        products: [
            [
                'id' => $productId,
                'order_minvol' => $minQty,
            ],
        ],
        offers: [
            [
                'id' => $offer1Id,
                'product_id' => $productId,
                'qty' => $qty1 + 1,
                'is_real_active' => $isRealActive1,
            ],
            [
                'id' => $offer2Id,
                'product_id' => $productId,
                'qty' => $qty2 + 1,
                'is_real_active' => $isRealActive2,
            ],
        ],
    );

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty1, $offer1Id)
        ->withItem($qty2, $offer2Id)
        ->withCustomer($basket->customer_id)
        ->make();

    $response = postJson("/api/v1/baskets/baskets/customer:set-item", $request);

    if ($status === 200) {
        $response->assertStatus($status)
            ->assertJsonPath('data', null);

        assertDatabaseHas(BasketItem::class, [
            "basket_id" => $basket->id,
            "offer_id" => $offer1Id,
            "qty" => $qty1,
        ]);
        assertDatabaseHas(BasketItem::class, [
            "basket_id" => $basket->id,
            "offer_id" => $offer2Id,
            "qty" => $qty2,
        ]);
    } else {
        $response->assertStatus($status)
            ->assertJsonPath('data', null)
            ->assertJsonPath('errors.0.code', "ValidateException");

        assertDatabaseMissing(BasketItem::class, [
            "basket_id" => $basket->id,
            "offer_id" => $offer1Id,
            "qty" => $qty1,
        ]);

        assertDatabaseMissing(BasketItem::class, [
            "basket_id" => $basket->id,
            "offer_id" => $offer2Id,
            "qty" => $qty2,
        ]);
    }
})->with([
    "success" => [],
    "isRealActive1" => [false, true, 400],
    "isRealActive2" => [true, false, 400],
    "false all" => [false, false, 400],
]);

test("POST /api/v1/baskets/baskets/customer:set-item change qty many offers in basket", function (
    bool $isRealActive1 = true,
    bool $isRealActive2 = true,
    int $status = 200
) {
    /** @var ApiV1ComponentTestCase|BasketSetItemTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    $offer1Id = 1;
    $offer2Id = 2;
    $productId = 2;
    $qty1 = 5.4;
    $qty2 = 10;
    $minQty = min($qty1, $qty2) - 1;

    $basket = Basket::factory()->create();
    BasketItem::factory()->for($basket)->create(['offer_id' => $offer1Id, 'qty' => $qty1]);
    BasketItem::factory()->for($basket)->create(['offer_id' => $offer2Id, 'qty' => $qty2]);

    $this->mockSetItem(
        products: [
            [
                'id' => $productId,
                'order_minvol' => $minQty,
            ],
        ],
        offers: [
            [
                'id' => $offer1Id,
                'product_id' => $productId,
                'qty' => $qty1 + 1,
                'is_real_active' => $isRealActive1,
            ],
            [
                'id' => $offer2Id,
                'product_id' => $productId,
                'qty' => $qty2 + 1,
                'is_real_active' => $isRealActive2,
            ],
        ],
    );

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty1 + 1, $offer1Id)
        ->withItem($qty2 + 1, $offer2Id)
        ->withCustomer($basket->customer_id)
        ->make();

    $response = postJson("/api/v1/baskets/baskets/customer:set-item", $request);

    if ($status === 200) {
        $response->assertStatus($status)
            ->assertJsonPath('data', null);

        assertDatabaseHas(BasketItem::class, [
            "basket_id" => $basket->id,
            "offer_id" => $offer1Id,
            "qty" => $qty1 + 1,
        ]);
        assertDatabaseHas(BasketItem::class, [
            "basket_id" => $basket->id,
            "offer_id" => $offer2Id,
            "qty" => $qty2 + 1,
        ]);
    } else {
        $response->assertStatus($status)
            ->assertJsonPath('data', null)
            ->assertJsonPath('errors.0.code', "ValidateException");

        assertDatabaseHas(BasketItem::class, [
            "basket_id" => $basket->id,
            "offer_id" => $offer1Id,
            "qty" => $qty1,
        ]);
        assertDatabaseHas(BasketItem::class, [
            "basket_id" => $basket->id,
            "offer_id" => $offer2Id,
            "qty" => $qty2,
        ]);
    }
})->with([
    "success" => [],
    "isRealActive1" => [false, true, 400],
    "isRealActive2" => [true, false, 400],
    "false all" => [false, false, 400],
]);

test("POST /api/v1/baskets/baskets/customer:set-item without basket success", function (
    bool $isRealActive = true,
    int $status = 200
) {
    /** @var ApiV1ComponentTestCase|BasketSetItemTestCase $this */
    $customerId = 3;
    $offerId = 1;
    $productId = 1;
    $qty = 10;

    $this->mockSetItem(
        products: [[
            'id' => $productId,
            'order_minvol' => $qty - 1,
        ]],
        offers: [[
            'id' => $offerId,
            'product_id' => $productId,
            'is_real_active' => $isRealActive,
            'qty' => $qty + 1,
        ]],
    );

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty, $offerId)
        ->withCustomer($customerId)
        ->make();

    $response = postJson("/api/v1/baskets/baskets/customer:set-item", $request);

    if ($status === 200) {
        $response->assertStatus($status)
            ->assertJsonPath('data', null);

        /** @var Basket $basket */
        $basket = Basket::query()->where('customer_id', $customerId)->first();
        $this->assertNotNull($basket);

        assertDatabaseHas(BasketItem::class, [
            "basket_id" => $basket->id,
            "offer_id" => $offerId,
            "qty" => $qty,
        ]);
    } else {
        $response->assertStatus($status)
            ->assertJsonPath('data', null)
            ->assertJsonPath('errors.0.code', "ValidateException");

        /** @var Basket $basket */
        $basket = Basket::query()->where('customer_id', $customerId)->first();
        $this->assertNull($basket);
    }
})->with([
    "success" => [],
    "error" => [false, 400],
]);

test("POST /api/v1/baskets/baskets/customer:set-item inactive product", function () {
    /** @var ApiV1ComponentTestCase|BasketSetItemTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    $offerId = 1;
    $productId = 1;
    $qty = 5.4;

    $this->mockSetItem(
        products: [],
        offers: [[
            'id' => $offerId,
            'product_id' => $productId,
            'qty' => $qty + 1,
        ]],
    );

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty, $offerId)
        ->withCustomer($basket->customer_id)
        ->make();

    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(400)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "ValidateException");
});

# todo: check stock
//test("POST /api/v1/baskets/baskets/customer:set-item less stock", function () {
//    /** @var ApiV1ComponentTestCase|BasketSetItemTestCase $this */
//    /** @var Basket $basket */
//    $basket = Basket::factory()->create();
//    $offerId = 1;
//    $productId = 1;
//    $qty = 5.4;
//
//$this->mockSetItem(
//    products: [['id' => $productId]],
//    offers: [[
//        'id' => $offerId,
//        'product_id' => $productId,
//        'qty' => $stockQty,
//    ]],
//);
//
//    $request = BasketCustomerSetItemRequestFactory::new()
//        ->withItem($qty, $offerId)
//        ->make(["customer_id" => $basket->customer_id,]);
//    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
//        ->assertStatus(400)
//        ->assertJsonPath('data', null)
//        ->assertJsonPath('errors.0.code', "ValidateException");
//});

test("POST /api/v1/baskets/baskets/customer:set-item less min qty", function () {
    /** @var ApiV1ComponentTestCase|BasketSetItemTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    $offerId = 1;
    $productId = 1;
    $qty = 5.4;

    $this->mockSetItem(
        products: [[
            'id' => $productId,
            'type' => ProductTypeEnum::WEIGHT,
            'order_minvol' => $qty + 1,
        ]],
        offers: [[
            'id' => $offerId,
            'product_id' => $productId,
        ]],
    );

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty, $offerId)
        ->make(["customer_id" => $basket->customer_id]);
    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(400);

    assertDatabaseMissing(BasketItem::class, [
        "basket_id" => $basket->id,
        "offer_id" => $offerId,
    ]);
});

test("POST /api/v1/baskets/baskets/customer:set-item minimum count check does not prevent deletion", function () {
    /** @var ApiV1ComponentTestCase|BasketSetItemTestCase $this */

    $offerId = 1;
    $productId = 1;
    $qty = 10;
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    BasketItem::factory()->for($basket)->create(['offer_id' => $offerId, 'qty' => $qty]);

    $this->mockSetItem(
        products: [[
            'id' => $productId,
            'type' => ProductTypeEnum::WEIGHT,
            'order_minvol' => 10,
        ]],
        offers: [[
            'id' => $offerId,
            'product_id' => $productId,
        ]],
    );

    $request = BasketCustomerSetItemRequestFactory::new()
        ->deleteItem($offerId)
        ->withCustomer($basket->customer_id)
        ->make();

    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseMissing(BasketItem::class, [
        "basket_id" => $basket->id,
        "offer_id" => $offerId,
    ]);
});

test("POST /api/v1/baskets/baskets/customer:set-item change qty for offer in basket", function (
    bool $isRealActive = true,
    int $status = 200
) {
    /** @var ApiV1ComponentTestCase|BasketSetItemTestCase $this */

    $offerId = 1;
    $productId = 1;
    $qty = 10;
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    BasketItem::factory()->for($basket)->create(['offer_id' => $offerId, 'qty' => $qty]);

    $this->mockSetItem(
        products: [[
            'id' => $productId,
            'type' => ProductTypeEnum::WEIGHT,
            'order_minvol' => 10,
        ]],
        offers: [[
            'id' => $offerId,
            'product_id' => $productId,
            'qty' => $qty + 1,
            'is_real_active' => $isRealActive,
        ]],
    );

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty + 1, $offerId)
        ->withCustomer($basket->customer_id)
        ->make();

    $response = postJson("/api/v1/baskets/baskets/customer:set-item", $request);

    if ($status === 200) {
        $response->assertStatus($status)
            ->assertJsonPath('data', null);

        assertDatabaseHas(BasketItem::class, [
            "basket_id" => $basket->id,
            "offer_id" => $offerId,
            "qty" => $qty + 1,
        ]);
    } else {
        $response->assertStatus($status)
            ->assertJsonPath('data', null)
            ->assertJsonPath('errors.0.code', "ValidateException");

        assertDatabaseHas(BasketItem::class, [
            "basket_id" => $basket->id,
            "offer_id" => $offerId,
            "qty" => $qty,
        ]);
    }
})->with([
    "success" => [],
    "error" => [false, 400],
]);

test("POST /api/v1/baskets/baskets/customer:set-item delete success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    /** @var BasketItem $basketItemDelete */
    $basketItemDelete = BasketItem::factory()->for($basket)->create();
    /** @var BasketItem $basketItemOk */
    $basketItemOk = BasketItem::factory()->for($basket)->create();

    $request = BasketCustomerSetItemRequestFactory::new()
        ->deleteItem($basketItemDelete->offer_id)
        ->withCustomer($basket->customer_id)
        ->make();

    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseHas(BasketItem::class, [
        'id' => $basketItemOk->id,
    ]);
    assertDatabaseMissing(BasketItem::class, [
        'id' => $basketItemDelete->id,
    ]);
});

test("POST /api/v1/baskets/baskets/customer:set-item delete many success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    /** @var BasketItem $basketItemDelete1 */
    $basketItemDelete1 = BasketItem::factory()->for($basket)->create();
    /** @var BasketItem $basketItemDelete2 */
    $basketItemDelete2 = BasketItem::factory()->for($basket)->create();

    $request = BasketCustomerSetItemRequestFactory::new()
        ->deleteItem($basketItemDelete1->offer_id)
        ->deleteItem($basketItemDelete2->offer_id)
        ->withCustomer($basket->customer_id)
        ->make();

    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseMissing(BasketItem::class, ['basket_id' => $basket->id]);
});

test("POST /api/v1/baskets/baskets/customer:set-item add and delete success", function () {
    /** @var ApiV1ComponentTestCase|BasketSetItemTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    /** @var BasketItem $basketItemDelete */
    $basketItemDelete = BasketItem::factory()->for($basket)->create();
    $offerId = 1;
    $productId = 1;
    $qty = 5.4;

    $this->mockSetItem(
        products: [[
            'id' => $productId,
            'order_minvol' => $qty - 1,
        ]],
        offers: [[
            'id' => $offerId,
            'product_id' => $productId,
            'qty' => $qty + 1,
        ]],
    );

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty, $offerId)
        ->deleteItem($basketItemDelete->offer_id)
        ->withCustomer($basket->customer_id)
        ->make();

    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseHas(BasketItem::class, [
        "basket_id" => $basket->id,
        "offer_id" => $offerId,
        "qty" => $qty,
    ]);
    assertDatabaseMissing(BasketItem::class, [
        'id' => $basketItemDelete->id,
    ]);
});

test("POST /api/v1/baskets/baskets/customer:set-item delete undefined success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();

    $request = BasketCustomerSetItemRequestFactory::new()
        ->deleteItem(1)
        ->withCustomer($basket->customer_id)
        ->make();

    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test("POST /api/v1/baskets/baskets/customer:set-item delete last item in basket success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    /** @var BasketItem $basketItemDelete */
    $basketItemDelete = BasketItem::factory()->for($basket)->create();

    $request = BasketCustomerSetItemRequestFactory::new()
        ->deleteItem($basketItemDelete->offer_id)
        ->withCustomer($basket->customer_id)
        ->make();

    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertModelMissing($basketItemDelete);
    assertModelMissing($basket);
});

test("POST /api/v1/baskets/baskets/customer:set-item delete last and add new item in basket success", function () {
    /** @var ApiV1ComponentTestCase|BasketSetItemTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    /** @var BasketItem $basketItemDelete */
    $basketItemDelete = BasketItem::factory()->for($basket)->create();
    $offerId = 1;
    $productId = 1;
    $qty = 5.4;

    $this->mockSetItem(
        products: [[
            'id' => $productId,
            'order_minvol' => $qty - 1,
        ]],
        offers: [[
            'id' => $offerId,
            'product_id' => $productId,
            'qty' => $qty + 1,
        ]],
    );

    $request = BasketCustomerSetItemRequestFactory::new()
        ->deleteItem($basketItemDelete->offer_id)
        ->withItem($qty, $offerId)
        ->withCustomer($basket->customer_id)
        ->make();

    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseMissing(BasketItem::class, [
        "id" => $basketItemDelete->id,
    ]);

    assertDatabaseHas(Basket::class, [
        "id" => $basket->id,
    ]);

    assertDatabaseHas(BasketItem::class, [
        "basket_id" => $basket->id,
        "offer_id" => $offerId,
        "qty" => $qty,
    ]);
});

test("DELETE /api/v1/baskets/baskets/customer:delete success", function () {
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    BasketItem::factory()->for($basket)->count(3)->create();

    deleteJson("/api/v1/baskets/baskets/customer:delete", [
        "customer_id" => $basket->customer_id,
    ])
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseMissing(BasketItem::class, [
        'basket_id' => $basket->id,
    ]);
});

test("DELETE /api/v1/baskets/baskets/customer:delete 404", function () {
    deleteJson("/api/v1/baskets/baskets/customer:delete", ["customer_id" => 2,])
        ->assertStatus(404)
        ->assertJsonPath('data', null);
});
