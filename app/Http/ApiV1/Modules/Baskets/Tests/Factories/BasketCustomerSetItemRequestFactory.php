<?php

namespace App\Http\ApiV1\Modules\Baskets\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class BasketCustomerSetItemRequestFactory extends BaseApiFactory
{
    protected array $items = [];
    protected ?int $customerId = null;

    protected function definition(): array
    {
        return [
            'customer_id' => $this->customerId ?? $this->faker->modelId(),
            'items' => $this->items ?: [
                'qty' => $this->faker->randomFloat(4),
                'offer_id' => $this->faker->modelId(),
            ],
        ];
    }

    public function withItem(float $qty, int $offerId): static
    {
        $this->items[] = ['qty' => $qty, 'offer_id' => $offerId];

        return $this;
    }

    public function withCustomer(int $customerId): static
    {
        $this->customerId = $customerId;

        return $this;
    }

    public function deleteItem(int $offerId): static
    {
        return $this->withItem(0, $offerId);
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
