<?php

use App\Domain\Baskets\Models\Basket;
use App\Domain\Baskets\Models\BasketItem;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/baskets/basket-items:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $basket = Basket::factory()->create();
    $basketItems = BasketItem::factory()->for($basket)->count(3)->create();
    $basketItems = $basketItems->sortBy('id');

    postJson("/api/v1/baskets/basket-items:search", [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $basketItems->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/baskets/basket-items:search filter success', function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
    ?bool $always,
) {
    FakerProvider::$optionalAlways = $always;

    $basket = Basket::factory()->create();
    /** @var BasketItem $model */
    $model = BasketItem::factory()->for($basket)->create($value ? [$fieldKey => $value] : []);

    postJson("/api/v1/baskets/basket-items:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id', null, null, null],
    ['basket_id', null, null, null, null],
    ['offer_id', null, null, null, true],
    ['product_id', null, null, null, true],
    ['qty', 123, 'qty_gte', 99],
    ['qty', 123, 'qty_lte', 150],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
], FakerProvider::$optionalDataset);

test('POST /api/v1/baskets/basket-items:search 400', function () {
    $basket = Basket::factory()->create();
    BasketItem::factory()->for($basket)->count(3)->create();

    postJson('/api/v1/baskets/basket-items:search', [
        "filter" => ["test" => true],
    ])->assertStatus(400);
});
