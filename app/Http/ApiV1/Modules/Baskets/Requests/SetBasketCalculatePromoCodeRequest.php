<?php

namespace App\Http\ApiV1\Modules\Baskets\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class SetBasketCalculatePromoCodeRequest extends BaseFormRequest implements BasketCustomerFilter
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
            'promo_code' => ['nullable', 'string'],
        ];
    }

    public function getCustomerId(): int
    {
        return $this->input('customer_id');
    }

    public function getPromoCode(): ?string
    {
        return $this->input('promo_code');
    }
}
