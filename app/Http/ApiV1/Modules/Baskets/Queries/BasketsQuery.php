<?php

namespace App\Http\ApiV1\Modules\Baskets\Queries;

use App\Domain\Baskets\Models\Basket;
use App\Http\ApiV1\Modules\Baskets\Requests\BasketCustomerFilter;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * @template T
 * @mixin T
 */
class BasketsQuery extends QueryBuilder
{
    public function __construct()
    {
        /** @var T */
        $query = Basket::query();

        parent::__construct($query);

        $this->allowedIncludes(['items']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('customer_id'),
            AllowedFilter::exact('promo_code'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->allowedSorts(['id', 'customer_id', 'promo_code', 'created_at', 'updated_at']);

        $this->defaultSort('id');
    }

    public function customer(BasketCustomerFilter $request): self
    {
        $this->subject->where('customer_id', $request->getCustomerId());

        return $this;
    }
}
