<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

/**
 * Параметры. Расшифровка значений:
 * * `basketStorageTime` - Время хранения корзины (час)
 */
enum SettingCodeEnum: string
{
    /** Время хранения корзины (час) */
    case BASKET_DURATION = 'basketStorageTime';
}
