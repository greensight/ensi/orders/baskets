<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public const DEFAULT_SELLER_ID = 1;

    public function up(): void
    {
        Schema::table('basket_items', function (Blueprint $table) {
            $table->dropColumn('seller_id');
        });
    }

    public function down(): void
    {
        Schema::table('basket_items', function (Blueprint $table) {
            $table->unsignedBigInteger('seller_id')->nullable();
        });

        DB::table('basket_items')->update(['seller_id' => self::DEFAULT_SELLER_ID]);

        Schema::table('basket_items', function (Blueprint $table) {
            $table->unsignedBigInteger('seller_id')->nullable(false)->change();
        });
    }
};
